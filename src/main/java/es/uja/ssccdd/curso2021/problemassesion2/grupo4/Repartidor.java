/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.TIEMPO_ESPERA_MIN;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.TIEMPO_ESPERA_MAX;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.PORCENTAJE_MINIMO;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final ArrayList<Plato> platos;
    private final ArrayList<MenuReparto> pedidos;
    private final ArrayList<Plato> platosPerdidos;
    private int platosProcesados;
    private boolean interrumpido;

    public Repartidor(int iD, ArrayList<Plato> platos, ArrayList<MenuReparto> pedido, ArrayList<Plato> platosPerdidos) {
        this.iD = iD;
        this.platos = platos;
        this.pedidos = pedido;
        this.interrumpido = false;
        this.platosPerdidos = platosPerdidos;
        this.platosProcesados = 0;
    }

    @Override
    public void run() {

        System.out.println("El repartidor " + iD + " ha comenzado a preparar los pedidos.");

        for (int i = 0; i < platos.size(); i++) {

            //Detecta si ha sido interrumpido y los pedidos han llegado al % minimo en caso contrario se procesa el siguiente 
            if (!(interrumpido && porcentajeCompletado() * 100 >= PORCENTAJE_MINIMO)) {
                //Se prueba a insertar un plato en los menus disponibles
                organizarPlato(i);

                //En cualquier caso, se marca como procesado
                platosProcesados++;

                try {
                    TimeUnit.SECONDS.sleep(Utils.random.nextInt(TIEMPO_ESPERA_MAX - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último plato, no lo contamos.
                    if (platos.size() != platosProcesados) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un plato en algún menú
     *
     * @param indicePlato indice del plato a insertar
     * @return true si ha podido ser insertado en algún menú
     */
    private boolean organizarPlato(int indicePlato) {

        boolean insertado = false;
        for (int j = 0; j < pedidos.size() && !insertado; j++) {
            if (pedidos.get(j).addPlato(platos.get(indicePlato))) {
                insertado = true;
            }
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!insertado) {
            platosPerdidos.add(platos.get(indicePlato));
        }

        return insertado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * platosProcesados / platos.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nRepartidor ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
        }

        mensaje.append(" ").append(platos.size()).append(" platos disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");

        for (MenuReparto pedido : pedidos) {
            mensaje.append("\n\t").append(pedido.toString());
        }

        if (platosProcesados < platos.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(platos.size() - platosProcesados).append(" platos por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
