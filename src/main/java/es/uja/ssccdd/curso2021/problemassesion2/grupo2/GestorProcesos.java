/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.NO_ASIGNADO;
import es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TipoPrioridad;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Runnable {
    private final int iD;
    private final ArrayList<Proceso> listaProcesos;
    private final ArrayList<ColaPrioridad> listaColasPrioridad;
    private final ArrayList<Proceso> listaNoAsignados;
    private int procesados;
    int ultimaCola; // ultima cola donde se asignó un proceso

    public GestorProcesos(int iD, ArrayList<Proceso> listaProcesos, ArrayList<ColaPrioridad> listaColasPrioridad, 
                          ArrayList<Proceso> listaNoAsignados) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.listaColasPrioridad = listaColasPrioridad;
        this.listaNoAsignados = listaNoAsignados;
        this.procesados = 0;
        this.ultimaCola = 0;
    }
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza ha asignar sus " + listaProcesos.size() +
                    " procesos");
        
        try {
            asignarProcesos();
            
            System.out.println("Ha finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } finally {
            // Se realiza antes de terminar pase lo que pase con la ejecución
            finalizar();
        }
    }
    
    public int getiD() {
        return iD;
    }
    
    /**
     * Se recorre la lista de procesos y se busca una cola donde asignarlo,
     * de forma equilibrada, si no es posible se anota.
     * @throws InterruptedException 
     */
    private void asignarProcesos() throws InterruptedException {
        
        for(Proceso proceso : listaProcesos) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Asignando " + proceso);
            // Asignamos el proceso a una cola si es posible
            boolean asignado = false;
            int colasRevisadas = 0;
            while( (colasRevisadas < listaColasPrioridad.size()) && !asignado ) {
                int indice = (ultimaCola + colasRevisadas) % listaColasPrioridad.size();
                if(listaColasPrioridad.get(indice).addProceso(proceso) != NO_ASIGNADO)
                    asignado = true;
                else
                    colasRevisadas++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // el último ordenador asignado
            if(asignado)
                ultimaCola = (ultimaCola + colasRevisadas + 1) % listaColasPrioridad.size();
            else 
                listaNoAsignados.add(proceso);
            
            // Simulamos por último el tiempo de asignación porque si se interrumpe
            // ya hemos completado la operación de asignación
            procesados++;
            TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoPrioridad()));
        }
        
    }
    
    /**
     * Antes de terminar la tarea añadirmos a la lista de procesos no asignados
     * los que no haya dado tiempo.
     */
    private void finalizar() {
        for( int i = 0 + procesados; i < listaProcesos.size(); i++)
            listaNoAsignados.add(listaProcesos.get(i));
    }
    
    private int tiempoAsignacion(TipoPrioridad tipoPrioridad) {
        return MAXIMO + tipoPrioridad.ordinal();
    }
}
