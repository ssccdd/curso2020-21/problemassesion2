/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo2;

import es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TipoPrioridad;

/**
 *
 * @author pedroj
 */
public class Proceso {
    private final int iD;
    private final TipoPrioridad tipoPrioridad;

    public Proceso(int iD, TipoPrioridad tipoPrioridad) {
        this.iD = iD;
        this.tipoPrioridad = tipoPrioridad;
    }

    public int getiD() {
        return iD;
    }

    public TipoPrioridad getTipoPrioridad() {
        return tipoPrioridad;
    }

    @Override
    public String toString() {
        return "Proceso{" + "iD=" + iD + ", tipoPrioridad=" + tipoPrioridad + '}';
    }
}
