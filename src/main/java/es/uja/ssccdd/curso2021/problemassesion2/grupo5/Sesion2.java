/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo5;

import es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.MODELOS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.IMPRESORAS_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.IMPRESORAS_A_GENERAR_MIN;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion2 {

    public static void main(String[] args) {

        // Variables aplicación
        int idImpresoras = 0;
        int idModelos = 0;
        int idIngenieros = 0;

        ArrayList<Thread> hilosIngenieros = new ArrayList<>();
        ArrayList<Modelo> modelosPerdidos = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {

            // Inicializamos las impresoras
            ArrayList<Impresora3D> listaImpresoras = new ArrayList<>();
            int impresorasAGenerar = random.nextInt(IMPRESORAS_A_GENERAR_MAX - IMPRESORAS_A_GENERAR_MIN) + IMPRESORAS_A_GENERAR_MIN;
            for (int j = 0; j < impresorasAGenerar; j++) {
                int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
                listaImpresoras.add(new Impresora3D(idImpresoras++, CalidadImpresion.getCalidad(aleatorioCalidad)));
            }

            // Inicializamos los modelos
            ArrayList<Modelo> listaModelos = new ArrayList<>();
            for (int j = 0; j < MODELOS_A_GENERAR; j++) {
                int aleatorioModelo = Utils.random.nextInt(VALOR_GENERACION);
                listaModelos.add(new Modelo(idModelos++, CalidadImpresion.getCalidad(aleatorioModelo)));
            }

            // Inicializamos los ingenieros
            Ingeniero ingeniero = new Ingeniero(idIngenieros++, listaModelos, listaImpresoras, modelosPerdidos);
            Thread thread = new Thread(ingeniero);
            thread.start();
            hilosIngenieros.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los trabajadores");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los trabajadores");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {
            hilosIngenieros.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a los trabajadores");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {
            try {
                hilosIngenieros.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + modelosPerdidos.size() + " modelos no han podido ser encolados:");
        for (Modelo p : modelosPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println(
                "HILO-Principal Ha finalizado la ejecución");
    }

}
