/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo5;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Runnable {

    private final int iD;
    private final ArrayList<Modelo> modelos;
    private final ArrayList<Impresora3D> impresoras;
    private final ArrayList<Modelo> modelosNoEncolados;
    private int modelosProcesados;
    private boolean interrumpido;
    private int siguienteImpresora;

    public Ingeniero(int iD, ArrayList<Modelo> modelos, ArrayList<Impresora3D> impresoras, ArrayList<Modelo> modelosNoEncolados) {
        this.iD = iD;
        this.modelos = modelos;
        this.impresoras = impresoras;
        this.modelosNoEncolados = modelosNoEncolados;
        this.modelosProcesados = 0;
        this.interrumpido = false;
        this.siguienteImpresora = 0;
    }

    @Override
    public void run() {

        System.out.println("El ingeniero " + iD + " ha comenzado a preparar los modelos.");

        for (int i = 0; i < modelos.size(); i++) {

            //Detecta si ha sido interrumpido y si lo ha sido, solo procesa los modelos médicos 
            if (!interrumpido || (interrumpido && modelos.get(i).getCalidadRequeridad() == Utils.CalidadImpresion.MEDICINA)) {
                //Se prueba a insertar un modelo en las impresoras disponibles
                encolarModelo(i);
                //En cualquier caso, se marca como procesado
                modelosProcesados++;

                try {
                    TimeUnit.SECONDS.sleep(modelos.get(i).getTiempoRequerido());
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último modelo, no lo contamos.
                    if (modelos.size() != modelosProcesados) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta encolar un modelo en alguna impresora
     *
     * @param indiceModelo indice del modelo a insertar
     * @return true si ha podido ser encolado
     */
    private boolean encolarModelo(int indiceModelo) {

        int impresorasComprobadas = 0; //Para evitar bucles infinitos
        boolean encolado = false;
        while (!encolado && impresorasComprobadas < impresoras.size()) {// Si no se ha generado una impresora de un tipo, esto evita el bucle infinito

            if (impresoras.get(siguienteImpresora).addModelo(modelos.get(indiceModelo))) {
                encolado = true;
            }

            impresorasComprobadas++;
            siguienteImpresora = (siguienteImpresora + 1) % impresoras.size();

        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!encolado) {
            modelosNoEncolados.add(modelos.get(indiceModelo));
        }

        return encolado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return porcentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * modelosProcesados / modelos.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nIngeniero ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
        }

        mensaje.append(" ").append(modelos.size()).append(" modelos disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");

        for (Impresora3D imp : impresoras) {
            mensaje.append("\n\t").append(imp.toString());
        }

        if (modelosProcesados < modelos.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(modelos.size() - modelosProcesados).append(" modelos por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
