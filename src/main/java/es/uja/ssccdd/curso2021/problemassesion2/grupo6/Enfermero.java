/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo6;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo6.Utils.PORCENTAJE_INYECTADAS_CONTINUAR;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo6.Utils.ESPERA_ENTRE_DOSIS_MAX;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo6.Utils.ESPERA_ENTRE_DOSIS_MIN;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo6.Utils.random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final ArrayList<DosisVacuna> dosisDisponibles;
    private final ArrayList<DosisVacuna> dosisDesaprovechadas;
    private final ArrayList<Paciente> pacientes;

    private int dosisUsadas;
    private int dosisPerdidas;
    private boolean interrumpido;

    private static int siguienteDosis = 0;

    public Enfermero(int iD, ArrayList<DosisVacuna> dosisDisponibles, ArrayList<DosisVacuna> dosisDesaprovechadas, ArrayList<Paciente> pacientes) {
        this.iD = iD;
        this.dosisDisponibles = dosisDisponibles;
        this.dosisDesaprovechadas = dosisDesaprovechadas;
        this.pacientes = pacientes;
        this.dosisUsadas = 0;
        this.dosisPerdidas = 0;
        this.interrumpido = false;
    }

    private static int getIndiceSiguienteDosis() {
        return siguienteDosis++;
    }

    @Override
    public void run() {

        System.out.println("El enfermero " + iD + " ha comenzado a vacunar pacientes.");

        for (int i = 0; (i < pacientes.size() * 2); i++) {

            //Detecta si ha sido interrumpido y si lo ha sido, solo procesa si se llevan procesadas mas del 80%
            if (!interrumpido || (interrumpido && porcentajeCompletado() * 100 >= PORCENTAJE_INYECTADAS_CONTINUAR)) {

                int dosisActual = getIndiceSiguienteDosis();
                inyectarDosis(dosisActual);

                //En cualquier caso, se marca como usada
                dosisUsadas++;

                try {
                    TimeUnit.SECONDS.sleep(random.nextInt(ESPERA_ENTRE_DOSIS_MAX - ESPERA_ENTRE_DOSIS_MIN) + ESPERA_ENTRE_DOSIS_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último paciente, no lo contamos.
                    if (pacientes.size() * 2 != dosisUsadas) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta vacunar un paciente con la siguiente dosis disponible
     *
     * @param dosisActual indice de la dosis a usar
     * @return true si ha podido ser inyectado en algún paciente
     */
    private boolean inyectarDosis(int dosisActual) {

        //Se prueba a inyectar una dosis a los pacientes disponibles
        int j = 0;
        boolean inyectada = false;

        while (j < pacientes.size() && !inyectada) {

            if (pacientes.get(j).addDosisVacuna(dosisDisponibles.get(dosisActual))) {
                inyectada = true;
            }

            j++;
        }
        //Si no se ha podido administrar se guarda la incidencia
        if (!inyectada) {
            dosisDesaprovechadas.add(dosisDisponibles.get(dosisActual));
            dosisPerdidas++;
        }

        return inyectada;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * dosisUsadas / (pacientes.size() * 2.0f);
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nEnfermero ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\" ");
        }

        mensaje.append("con ").append(pacientes.size()).append(" pacientes, ").append(dosisUsadas - dosisPerdidas).append(" dosis inyectadas y ").append(dosisPerdidas).append(" dosis perdidas.");

        for (Paciente pac : pacientes) {
            mensaje.append("\n\t").append(pac.toString());
        }

        return mensaje.toString();
    }

}
