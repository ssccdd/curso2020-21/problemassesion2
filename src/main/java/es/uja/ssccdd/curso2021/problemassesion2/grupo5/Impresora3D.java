/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo5;

import es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.TAMAÑO_MAXIMO_COLA;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Impresora3D {

    private final int iD;
    private final CalidadImpresion calidadImpresion;
    private final ArrayList<Modelo> colaImpresion;

    /**
     * Genera una nueva impresora 3D
     *
     * @param iD id de la impresora
     * @param calidadMaxima calidad de impresión que puede alcanzar la impresora
     */
    public Impresora3D(int iD, CalidadImpresion calidadMaxima) {
        this.iD = iD;
        this.calidadImpresion = calidadMaxima;
        this.colaImpresion = new ArrayList<>();
    }

    public int getiD() {
        return iD;
    }

    public CalidadImpresion getCalidadImpresion() {
        return calidadImpresion;
    }

    /**
     * Asigna un nuevo modelo a la cola de impresión
     *
     * @param modelo para añadir a la lista
     * @return true si se ha añadido false si no tiene las capacidades adecuadas
     */
    public boolean addModelo(Modelo modelo) {
        boolean resultado = false;

        if (modelo.getCalidadRequeridad().equals(calidadImpresion) && colaImpresion.size() < TAMAÑO_MAXIMO_COLA) {
            colaImpresion.add(modelo);
            resultado = true;
        }

        return resultado;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();

        resultado.append("Impresora[").append(iD).append(", calidad=").append(calidadImpresion).append(", enEspera='").append(colaImpresion.size()).append("']{Modelos=[");

        for (Modelo modelo : colaImpresion) {
            resultado.append(modelo.toString()).append(" -> ");
        }

        resultado.append("FIN]}");

        return resultado.toString();
    }

}
