/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MAXIMO;
import es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.TipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.UMBRAL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorAlmacenamiento implements Runnable {
    private final int iD;
    private final ArrayList<Archivo> listaArchivos;
    private final ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    private final ArrayList<Archivo> listaNoAsignados;
    private int procesados;

    public GestorAlmacenamiento(int iD, ArrayList<Archivo> listaArchivos, ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento, 
                                ArrayList<Archivo> listaNoAsignados) {
        this.iD = iD;
        this.listaArchivos = listaArchivos;
        this.listaDeAlmacenamiento = listaDeAlmacenamiento;
        this.listaNoAsignados = listaNoAsignados;
    }
    
    

    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza ha asignar sus " + listaArchivos.size() +
                    " archivos");
        
        try {
            asignarArchivos();
            
            System.out.println("Ha finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } finally {
            // Se realiza antes de terminar pase lo que pase con la ejecución
            finalizar();
        }
    }
    
    public int getiD() {
        return iD;
    }
    
    /**
     * Se recorre la lista de archivos y se busca la primera unidad de almacenamiento
     * para asignarlo si no es posible se anota.
     * @throws InterruptedException 
     */
    private void asignarArchivos() throws InterruptedException {
        // umbral para aceptar la interrupción de la tarea
        int umbral = (listaArchivos.size() * UMBRAL) / 100;
        
        for(Archivo archivo : listaArchivos) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de archivo y la compara con el umbral de interrupción.
            if ( Thread.interrupted() )
                if ( procesados < umbral )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " El gestor no puede ser interrumpido");

            
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Asignando " + archivo);
            // Asignamos el archivo a la primera unidad de almacenamiento posible
            boolean asignado = false;
            int indice = 0;
            while( (indice < listaDeAlmacenamiento.size()) && !asignado ) {
                if(listaDeAlmacenamiento.get(indice).addArchivo(archivo) != ESPACIO_INSUFICIENTE)
                    asignado = true;
                else
                    indice++;
            }
            
            // Si no se ha podido almacenar el archivo se anota
            if(!asignado)
                listaNoAsignados.add(archivo);
            
            // Simulamos por último el tiempo de asignación
            procesados++;
            try {
                TimeUnit.SECONDS.sleep(tiempoAsignacion(archivo.getTipoArchivo()));
            } catch (InterruptedException ex) {
                // Si se produce la interrupción mientas está suspendido hay que tratarla
                // por si ya se ha superado el umbral permitido para la interrupción
                if ( procesados < umbral )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " El gestor no puede ser interrumpido");
            }
        }
    }
    
    /**
     * Antes de terminar la tarea añadirmos a la lista de archivos no asignados
     * los que no hayan sido procesados por el gestor.
     */
    private void finalizar() {
        for( int i = 0 + procesados; i < listaArchivos.size(); i++)
            listaNoAsignados.add(listaArchivos.get(i));
    }
    
    private int tiempoAsignacion(TipoArchivo tipoArchivo) {
        return MAXIMO + tipoArchivo.ordinal();
    }
}
