/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.TIEMPO_REQUERIDO_MAX;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.TIEMPO_REQUERIDO_MIN;
import es.uja.ssccdd.curso2021.problemassesion2.grupo5.Utils.CalidadImpresion;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Modelo {

    private final int iD;
    private final CalidadImpresion calidadRequeridad;
    private final int tiempoRequerido;

    public Modelo(int iD, CalidadImpresion calidadRequeridad) {
        this.iD = iD;
        this.calidadRequeridad = calidadRequeridad;
        this.tiempoRequerido = random.nextInt(TIEMPO_REQUERIDO_MAX - TIEMPO_REQUERIDO_MIN) + TIEMPO_REQUERIDO_MIN;
    }

    public int getiD() {
        return iD;
    }

    public CalidadImpresion getCalidadRequeridad() {
        return calidadRequeridad;
    }

    public int getTiempoRequerido() {
        return tiempoRequerido;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        return "Modelo{" + "iD=" + iD + ", calidad requerida=" + calidadRequeridad + "}";
    }
}
