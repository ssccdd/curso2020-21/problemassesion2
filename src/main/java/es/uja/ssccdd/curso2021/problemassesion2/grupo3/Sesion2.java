/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MAXIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MAX_ARCHIVOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MINIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.MIN_ARCHIVOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.TIEMPO_SUSPENSION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.TipoArchivo.getTipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        int iD;
        Thread[] listaHilos;
        GestorAlmacenamiento gestor;
        ArrayList<Archivo>[] listaArchivos;
        ArrayList<UnidadAlmacenamiento>[] listaDeAlmacenamiento;
        ArrayList<Archivo>[] listaNoAsignados;
        int capacidad; // Capacidad de almacenamiento de la unidad
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
    
        // Inicializamos las variables del sistema
        iD = 0;
        listaArchivos = new ArrayList[NUM_GESTORES];
        listaDeAlmacenamiento = new ArrayList[NUM_GESTORES];
        listaNoAsignados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        for(int i = 0; i < NUM_GESTORES; i++) {
            // Creamos las listas asociadas al gestor i
            listaArchivos[i] = new ArrayList();
            listaDeAlmacenamiento[i] = new ArrayList();
            listaNoAsignados[i] = new ArrayList();
            
            // Archivos para el gestor i
            int totalArchivos = MIN_ARCHIVOS + aleatorio.nextInt(MAX_ARCHIVOS - MIN_ARCHIVOS + 1);
            for(int j = 0 ; j < totalArchivos; j++) {
                listaArchivos[i].add(new Archivo(iD, getTipoArchivo(aleatorio.nextInt(VALOR_CONSTRUCCION))));
                iD++;
            }
                
            // Unidades de almacenamiento para el gestor i
            int totalUnidadesAlmacenamiento = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            for(int j = 0; j < totalUnidadesAlmacenamiento; j++) {
                // Generamos la capacidad de la unidad
                capacidad = MINIMO_ALMACENAMIENTO + aleatorio.nextInt(MAXIMO_ALMACENAMIENTO - 
                                                                      MINIMO_ALMACENAMIENTO + 1);
            
                listaDeAlmacenamiento[i].add(new UnidadAlmacenamiento(iD,capacidad));
                iD++;
            }
            
            gestor = new GestorAlmacenamiento(i, listaArchivos[i], listaDeAlmacenamiento[i], listaNoAsignados[i]);
            // Asociamos el gestor a su hilo
            listaHilos[i] = new Thread(gestor, "GestorAlmacenaminento("+ gestor.getiD() + ")");
        }
    
        // Ejecutamos los hilos
        for(Thread hilo : listaHilos)
            hilo.start();
        
        // Suspendemos el hilo principal por un tiempo establecido
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de los gestores");
        TimeUnit.MINUTES.sleep(TIEMPO_SUSPENSION);

        // Se solicita la cancelación de los gestores que no hayan finalizado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();

        // Mostrar el estado de sistema almacenamiento
        System.out.println("(HILO_PRINCIPAL) Estado del sitema almacenamiento");
        
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("******************** Gestor(" + i + ") ********************");
            for(UnidadAlmacenamiento unidadAlmacenamiento : listaDeAlmacenamiento[i])
                System.out.println(unidadAlmacenamiento);
            
            System.out.println("Procesos no asignados " + listaNoAsignados[i]);
        }
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
