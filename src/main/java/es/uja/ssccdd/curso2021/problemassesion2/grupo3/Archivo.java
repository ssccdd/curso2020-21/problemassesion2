/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo3;

import es.uja.ssccdd.curso2021.problemassesion2.grupo3.Constantes.TipoArchivo;

/**
 *
 * @author pedroj
 */
public class Archivo {
    private final int iD;
    private final TipoArchivo tipoArchivo;

    public Archivo(int iD, TipoArchivo tipoArchivo) {
        this.iD = iD;
        this.tipoArchivo = tipoArchivo;
    }

    public int getiD() {
        return iD;
    }

    public TipoArchivo getTipoArchivo() {
        return tipoArchivo;
    }

    @Override
    public String toString() {
        return "Archivo{" + "iD=" + iD + ", tipoArchivo=" + tipoArchivo + '}';
    }
}
