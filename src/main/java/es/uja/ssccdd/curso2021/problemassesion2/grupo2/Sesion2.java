/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.MAX_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.MIN_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TIEMPO_SUSPENSION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TIPOS_PRIORIDAD;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TipoPrioridad.getTipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        int iD;
        Thread[] listaHilos;
        GestorProcesos gestor;
        ArrayList<Proceso>[] listaProcesos;
        ArrayList<ColaPrioridad>[] listaColasPrioridad;
        ArrayList<Proceso>[] listaNoAsignados;
        int[] capacidad; // Capacidad de almacenamiento para las prioridades
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
    
        // Inicializamos las variables del sistema
        iD = 0;
        listaProcesos = new ArrayList[NUM_GESTORES];
        listaColasPrioridad = new ArrayList[NUM_GESTORES];
        listaNoAsignados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        capacidad = new int[TIPOS_PRIORIDAD];
        for(int i = 0; i < NUM_GESTORES; i++) {
            // Creamos las listas asociadas al gestor i
            listaProcesos[i] = new ArrayList();
            listaColasPrioridad[i] = new ArrayList();
            listaNoAsignados[i] = new ArrayList();
            
            // Procesos para el gestor i
            int totalProcesos = MIN_PROCESOS + aleatorio.nextInt(MAX_PROCESOS - MIN_PROCESOS + 1);
            for(int j = 0 ; j < totalProcesos; j++) {
                listaProcesos[i].add(new Proceso(iD, getTipoPrioridad(aleatorio.nextInt(VALOR_CONSTRUCCION))));
                iD++;
            }
                
            // Colas prioridad para el gestor i
            int totalColasPrioridad = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            for(int j = 0; j < totalColasPrioridad; j++) {
                // Generamos la capacidad del ordenador
                for(int k = 0; k < TIPOS_PRIORIDAD; k++) 
                    capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
                listaColasPrioridad[i].add(new ColaPrioridad(iD,capacidad));
                iD++;
            }
            
            gestor = new GestorProcesos(i, listaProcesos[i], listaColasPrioridad[i], listaNoAsignados[i]);
            // Asociamos el gestor a su hilo
            listaHilos[i] = new Thread(gestor, "GestorProcesos("+ gestor.getiD() + ")");
        }
    
        // Ejecutamos los hilos
        for(Thread hilo : listaHilos)
            hilo.start();
        
        // Suspendemos el hilo principal por un tiempo establecido
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de los gestores");
        TimeUnit.MINUTES.sleep(TIEMPO_SUSPENSION);

        // Se solicita la cancelación de los gestores que no hayan finalizado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();

        // Mostrar el estado de sistema de prioridad
        System.out.println("(HILO_PRINCIPAL) Estado del sitema de prioridad");
        
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("******************** Gestor(" + i + ") ********************");
            for(ColaPrioridad colaPrioridad : listaColasPrioridad[i])
                System.out.println(colaPrioridad);
            
            System.out.println("Procesos no asignados " + listaNoAsignados[i]);
        }
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
