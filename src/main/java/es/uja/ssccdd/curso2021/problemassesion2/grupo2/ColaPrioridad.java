/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TIPOS_PRIORIDAD;
import es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo2.Constantes.TipoPrioridad.BAJA;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class ColaPrioridad {
    private final int iD;
    private final Proceso[][] colaProcesos;

    /**
     * @param iD identificador de la cola con prioridad
     * @param capacidad mámimo número de procesos de cada tipo de prioridad que
     *                  puede albergar la cola con prioridad
     */
    public ColaPrioridad(int iD, int[] capacidad) {
        this.iD = iD;
        
        this.colaProcesos = new Proceso[TIPOS_PRIORIDAD][];
        for(int i = 0; i < TIPOS_PRIORIDAD; i++)
            this.colaProcesos[i] = new Proceso[capacidad[i]];
    }

    public int getiD() {
        return iD;
    }
    
    /**
     * Se añade un proceso, según su prioridad, a la cola con prioridad si hay espacio
     * o en una prioridad inferior
     * @param proceso proceso que se añade a la cola con prioridad
     * @return NO_ASIGNADO si no se ha podido añadir el proceso a la cola con prioridad
     */
    public int addProceso(Proceso proceso) {
        int resultado = NO_ASIGNADO;
        int prioridad = proceso.getTipoPrioridad().ordinal();

        // Empezando por su prioridad original buscamos el primer hueco para incluir
        // el proceso, si es posible
        boolean encontrado = false;
        while( (prioridad >= BAJA.ordinal()) && !encontrado ) {
            int i = 0;
            while( (i < colaProcesos[prioridad].length) && !encontrado )
                if( colaProcesos[prioridad][i] == null ) {
                    colaProcesos[prioridad][i] = proceso;
                    resultado = colaProcesos[prioridad].length - i; // capacidad que queda
                    encontrado = true;
                } else
                    i++;
            prioridad--;
        }
        
        return resultado;
    }
        
    @Override
    public String toString() {
        String resultado = "ColaPrioridad(" + "iD_" + iD + ") procesos almacenados\n";
        
        for(int i = 0; i < TIPOS_PRIORIDAD; i++)
            resultado = resultado + "\t" + TipoPrioridad.values()[i] + "="+ Arrays.toString(colaProcesos[i]) + "\n"; 
        
        return resultado;
    }   
}
