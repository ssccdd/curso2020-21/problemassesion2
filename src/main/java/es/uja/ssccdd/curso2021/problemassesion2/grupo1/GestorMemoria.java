/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MEMORIA_COMPLETA;
import es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.TipoProceso;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorMemoria implements Runnable {
    private final int iD;
    private final ArrayList<Proceso> listaProcesos;
    private final ArrayList<Ordenador> listaOrdenadores;
    private final ArrayList<Proceso> listaNoAsignados;
    private int procesados;
    int ultimoOrdenador; // último ordenador donde se asignó un proceso

    public GestorMemoria(int iD, ArrayList<Proceso> listaProcesos, ArrayList<Ordenador> listaOrdenadores, 
                         ArrayList<Proceso> listaNoAsignados) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.listaOrdenadores = listaOrdenadores;
        this.listaNoAsignados = listaNoAsignados;
        this.procesados = 0;
        this.ultimoOrdenador = 0;
    }
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza ha asignar sus " + listaProcesos.size() +
                    " procesos");
        
        try {
            asignarProcesos();
            
            System.out.println("Ha finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } finally {
            // Se realiza antes de terminar pase lo que pase con la ejecución
            finalizar();
        }
    }

    public int getiD() {
        return iD;
    }
    
    /**
     * Se recorre la lista de procesos y se busca un ordenador donde asignarlo,
     * de forma equilibrada, si no es posible se anota.
     * @throws InterruptedException 
     */
    private void asignarProcesos() throws InterruptedException {
     
        for(Proceso proceso : listaProcesos) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Asignando " + proceso);
            // Asignamos el proceso a un ordenador si es posible
            boolean asignado = false;
            int ordenadoresRevisados = 0;
            while( (ordenadoresRevisados < listaOrdenadores.size()) && !asignado ) {
                int indice = (ultimoOrdenador + ordenadoresRevisados) % listaOrdenadores.size();
                if(listaOrdenadores.get(indice).addProceso(proceso) != MEMORIA_COMPLETA)
                    asignado = true;
                else
                    ordenadoresRevisados++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // el último ordenador asignado
            if(asignado)
                ultimoOrdenador = (ultimoOrdenador + ordenadoresRevisados + 1) % listaOrdenadores.size();
            else 
                listaNoAsignados.add(proceso);
            
            // Simulamos por último el tiempo de asignación porque si se interrumpe
            // ya hemos completado la operación de asignación
            procesados++;
            TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoProceso()));
        }
        
    }
    
    /**
     * Antes de terminar la tarea añadirmos a la lista de procesos no asignados
     * los que no hayan sido procesados por el gestor.
     */
    private void finalizar() {
        for( int i = 0 + procesados; i < listaProcesos.size(); i++)
            listaNoAsignados.add(listaProcesos.get(i));
    }
    
    private int tiempoAsignacion(TipoProceso tipoProceso) {
        return MAXIMO + tipoProceso.ordinal();
    }
}
