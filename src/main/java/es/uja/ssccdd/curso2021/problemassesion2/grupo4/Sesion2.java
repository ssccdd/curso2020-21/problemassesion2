/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo4;

import es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.MENUS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.PLATOS_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.PLATOS_A_GENERAR_MIN;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.REPARTIDORES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.VALOR_GENERACION;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion2 {

    public static void main(String[] args) {
        // Variables aplicación
        int idMenus = 0;
        int idPlatos = 0;
        int idRepartidores = 0;

        ArrayList<Thread> hilosRepartidores = new ArrayList<>();
        ArrayList<Plato> platosPerdidos = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println(
                "HILO-Principal Ha iniciado la ejecución");

        for (int i = 0;
                i < REPARTIDORES_A_GENERAR;
                i++) {

            // Inicializamos los menús
            ArrayList<MenuReparto> listaPedidos = new ArrayList<>();
            for (int j = 0; j < MENUS_A_GENERAR; j++) {
                listaPedidos.add(new MenuReparto(idMenus++));
            }

            // Inicializamos los platos
            int platosAGenerar = random.nextInt(PLATOS_A_GENERAR_MAX - PLATOS_A_GENERAR_MIN) + PLATOS_A_GENERAR_MIN;
            ArrayList<Plato> listaPlatos = new ArrayList<>();
            for (int j = 0; j < platosAGenerar; j++) {
                int aleatorioPlato = Utils.random.nextInt(VALOR_GENERACION);
                listaPlatos.add(new Plato(idPlatos++, TipoPlato.getPlato(aleatorioPlato)));
            }

            // Inicializamos los repartidores
            Repartidor repartidor = new Repartidor(idRepartidores++, listaPlatos, listaPedidos, platosPerdidos);
            Thread thread = new Thread(repartidor);
            thread.start();
            hilosRepartidores.add(thread);

        }

        System.out.println(
                "HILO-Principal Espera a la finalización de los repartidores");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(
                "HILO-Principal Solicita la finalización de los repartidores");

        for (int i = 0;
                i < REPARTIDORES_A_GENERAR;
                i++) {
            hilosRepartidores.get(i).interrupt();
        }

        System.out.println(
                "HILO-Principal Espera a los repartidores");

        for (int i = 0;
                i < REPARTIDORES_A_GENERAR;
                i++) {
            try {
                hilosRepartidores.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + platosPerdidos.size() + " platos no han podido ser insertados:");
        for (Plato p : platosPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println(
                "HILO-Principal Ha finalizado la ejecución");
    }

}
