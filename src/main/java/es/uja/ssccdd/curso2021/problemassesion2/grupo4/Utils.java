/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo4;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int MENUS_A_GENERAR = 10;
    public static final int REPARTIDORES_A_GENERAR = 8;
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_TIPOS_PLATOS = TipoPlato.values().length;
    public static final int PLATOS_A_GENERAR_MIN = 10;
    public static final int PLATOS_A_GENERAR_MAX = 30;
    public static final int TIEMPO_ESPERA_MIN = 1;
    public static final int TIEMPO_ESPERA_MAX = 4;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 30;
    public static final int PORCENTAJE_MINIMO = 75;

    //Enumerado para el tipo de plato
    public enum TipoPlato {
        PRINCIPAL(50), SEGUNDO(75), POSTRE(100);

        private final int valor;

        private TipoPlato(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un plato relacionado con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación del plato
         * @return el TipoPlato con el valor de generación
         */
        public static TipoPlato getPlato(int valor) {
            TipoPlato resultado = null;
            TipoPlato[] platos = TipoPlato.values();
            int i = 0;

            while ((i < platos.length) && (resultado == null)) {
                if (platos[i].valor >= valor) {
                    resultado = platos[i];
                }

                i++;
            }

            return resultado;
        }
    }

}
