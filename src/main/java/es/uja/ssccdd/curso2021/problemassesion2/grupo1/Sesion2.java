/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion2.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MAX_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.MIN_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.TIEMPO_SUSPENSION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.TIPOS_PROCESO;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.TipoProceso.getTipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion2.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        int iD;
        Thread[] listaHilos;
        GestorMemoria gestor;
        ArrayList<Proceso>[] listaProcesos;
        ArrayList<Ordenador>[] listaOrdenadores;
        ArrayList<Proceso>[] listaNoAsignados;
        int[] capacidad; // Capacidad de almacenamiento para los ordenadores
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
    
        // Inicializamos las variables del sistema
        iD = 0;
        listaProcesos = new ArrayList[NUM_GESTORES];
        listaOrdenadores = new ArrayList[NUM_GESTORES];
        listaNoAsignados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        capacidad = new int[TIPOS_PROCESO];
        for(int i = 0; i < NUM_GESTORES; i++) {
            // Creamos las listas asociadas al gestor i
            listaProcesos[i] = new ArrayList();
            listaOrdenadores[i] = new ArrayList();
            listaNoAsignados[i] = new ArrayList();
            
            // Procesos para el gestor i
            int totalProcesos = MIN_PROCESOS + aleatorio.nextInt(MAX_PROCESOS - MIN_PROCESOS + 1);
            for(int j = 0 ; j < totalProcesos; j++) {
                listaProcesos[i].add(new Proceso(iD, getTipoProceso(aleatorio.nextInt(VALOR_CONSTRUCCION))));
                iD++;
            }
                
            // Ordenadores para el gestor i
            int totalOrdenadores = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            for(int j = 0; j < totalOrdenadores; j++) {
                // Generamos la capacidad del ordenador
                for(int k = 0; k < TIPOS_PROCESO; k++) 
                    capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
                listaOrdenadores[i].add(new Ordenador(iD,capacidad));
                iD++;
            }
            
            gestor = new GestorMemoria(i, listaProcesos[i], listaOrdenadores[i], listaNoAsignados[i]);
            // Asociamos el gestor a su hilo
            listaHilos[i] = new Thread(gestor, "GestorMemoria("+ gestor.getiD() + ")");
        }
    
        // Ejecutamos los hilos
        for(Thread hilo : listaHilos)
            hilo.start();
        
        // Suspendemos el hilo principal por un tiempo establecido
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de los gestores");
        TimeUnit.MINUTES.sleep(TIEMPO_SUSPENSION);

        // Se solicita la cancelación de los gestores que no han terminado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();

        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del sitema distribuido");
        
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("******************** Gestor(" + i + ") ********************");
            for(Ordenador ordenador : listaOrdenadores[i])
                System.out.println(ordenador);
            
            System.out.println("Procesos no asignados " + listaNoAsignados[i]);
        }
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }  
}
