[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 2

Problemas propuestos para la Sesión 2 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Los **objetivos** de la práctica son:


- Definir adecuadamente las clase que representa a lo que se ejcutará en el _hilo_. Programando adecuadamente la excepción de interrupción de su ejecución.
- Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
    - Crear los objetos que representan lo que deberá ejecutarse en los _hilos_.
    - Crear los objetos de los _hilos_ de ejecución. Asociando correctamente lo que deberá ejercutarse.
    - La interrupción de los _hilos_ cuando sea solicitado.

Los ejercicios son diferentes para cada grupo:
- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion2/-/blob/master/README.md#grupo-6)

### Grupo 1

Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio.

Hay que completar la implementación de las siguientes clases:

 - `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. La clase tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Lista de procesos que tendrá que asignar a un ordenador.
     - Lista de ordenadores asignados.
	 - Lista de procesos no asignados a ningún ordenador.

	La tarea que debe realizar es la siguiente:
	 - Para cada `Proceso` se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
	 - Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados.
	 - Si se solicita la interrupción de la tarea, como mínimo deberá completarse la última asignación en curso. El resto de procesos deberán añadirse a la lista de procesos no asignados.

- `Hilo Principal` : Deberá completar los siguientes pasos:
	- Crea un número de gestores de memoria que estarán determinados por una constante.
		- Cada gestor deberá tener asociada una lista de procesos aleatoria comprendido entre un valor máximo y mínimo que está recogido en las constantes. El tipo de proceso también será aleatorio.
		- Cada gestor tendrá un valor variable de ordenadores asignados entre 2 y 5.
	-	Crea los hilos asociados a los gestores de memoria y comienza su ejecución.
	-	Se suspenderá por un tiempo determinado en minutos cuyo valor está en las constantes.
	-	Pasado ese tiempo solicitará la interrupción de todos los hilos y esperará a que finalicen.
	-	Presentará un informe donde se presente la asignación que cada uno de los gestores ha hecho de los procesos asociados a sus ordenadores.
	-	Además se presentará la lista de todos los procesos no asignados por cada uno de los gestores.

### Grupo 2

Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio.

Hay que completar la implementación de las siguientes clases:

 - `GestorProcesos` : Es el encargado de asignar un `Proceso` a una de las colas de prioridad que tiene asignadas. La clase tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Lista de procesos que tendrá que asignar a una cola de prioridad.
     - Lista de colas de prioridad asignadas.
	 - Lista de procesos no asignados a ninguna cola de prioridad.

	La tarea que debe realizar es la siguiente:
	 - Para cada `Proceso` se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre las diferentes colas de prioridad. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para completar esta operación.
	 - Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados.
	 - Si se solicita la interrupción de la tarea, como mínimo deberá completarse la última asignación en curso. El resto de procesos deberán añadirse a la lista de procesos no asignados.

- `Hilo Principal` : Deberá completar los siguientes pasos:
	- Crea un número de gestores de procesos que estarán determinados por una constante.
		- Cada gestor deberá tener asociada una lista de procesos aleatoria comprendido entre un valor máximo y mínimo que está recogido en las constantes. El tipo de proceso también será aleatorio.
		- Cada gestor tendrá un valor variable de colas de prioridad entre 2 y 5.
	-	Crea los hilos asociados a los gestores de procesos y comienza su ejecución.
	-	Se suspenderá por un tiempo determinado en minutos cuyo valor está en las constantes.
	-	Pasado ese tiempo solicitará la interrupción de todos los hilos y esperará a que finalicen.
	-	Presentará un informe donde se presente la asignación que cada uno de los gestores ha hecho de los procesos asociados a sus colas de prioridad.
	-	Además se presentará la lista de todos los procesos no asignados por cada uno de los gestores.

### Grupo 3

Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio.

Hay que completar la implementación de las siguientes clases:

 - `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una de las unidades de almacenamiento que tiene asignadas. La clase tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Lista de archivos que tendrá que asignar a una unidad de almacenamiento.
     - Lista de unidades de almacenamiento asignadas.
	 - Lista de archivos no asignados a ninguna unidad de almacenamiento.

	La tarea que debe realizar es la siguiente:
	 - Para cada `Archivo` se buscará la primera unidad de almacenamiento disponible para asignarlo. Se simulará un tiempo aleatorio, atendiendo al tipo de archivo, para esta operación.
	 - Si no ha podido ser asignado el archivo se añadirá a la lista de archivos no asignados.
	 - Si se solicita la interrupción de la tarea, y se ha alcanzado el umbral de los archivos ya procesados, deberá procesar la lista completa de archivos. En otro caso los archivos restantes serán añadidos a la lista de archivos no asignados.

- `Hilo Principal` : Deberá completar los siguientes pasos:
	- Crea un número de gestores de almacenamiento que estarán determinados por una constante.
		- Cada gestor deberá tener asociada una lista de archivos aleatoria comprendido entre un valor máximo y mínimo que está recogido en las constantes. El tipo de archivo también será aleatorio.
		- Cada gestor tendrá un valor variable de unidades de almacenamiento entre 2 y 5.
	-	Crea los hilos asociados a los gestores de almacenamiento y comienza su ejecución.
	-	Se suspenderá por un tiempo determinado en minutos cuyo valor está en las constantes.
	-	Pasado ese tiempo solicitará la interrupción de todos los hilos y esperará a que finalicen.
	-	Presentará un informe donde se presente la asignación que cada uno de los gestores ha hecho de los archivos asociados a sus unidades de almacenamiento.
	-	Además se presentará la lista de todos los archivos no asignados por cada uno de los gestores.

### Grupo 4
En el ejercicio se pretende simular la preparación de diferentes menus para su reparto, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá el proceso de organización. Para la solución se tiene que utilizar los siguientes elementos ya programados:
- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato` pero no mas de uno de cada tipo.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Repartidor`: Representa a un trabajador que debe realizar una ordenación de los platos en los pedidos. Tendrá al menos  las siguientes variables de instancia:
 - Un identificador.
 - Una cantidad de menus a los que añadir platos.
 - Una lista de platos para organizar en menús.
 - Los pasos que debe realizar son:
	 - Preparar el pedido: 
		 - La preparación podrá interrumpirse, cancelarse, pero no antes de procesar el 75% de los platos. Si se cancela antes deberá seguir trabajando hasta llegar al 75%.
		 - Procesa un plato: lo inserta dentro de un menú si es posible, intentando maximizar el número de pedidos completos. Si no se puede, registrará que platos no han podido ser incluidos, en una estructura de datos común a todos los hilos.
		 - El plato se considera procesado se pueda o no insertar en algún menú.
		 - Se simula un tiempo para organizar el plato que estará comprendido entre 1 y 4 segundos.
		 - A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por el repartidor, debe incluir como mínimo el porcentaje de platos procesados, la información de los menús y debe quedar claro si el pedido ha sido cancelado/interrumpido. 
		 - Los platos no imprimidos por falta de tiempo **no se guardaran** en la estructura común.
- `Hilo principal`: Realizará los siguientes pasos:
	- Crear una estructura para guardar los platos que no han podido ser incluidos en ningún menú.
	- Crear 8 repartidores:
		- Asignar a cada repartidor 10 menús, entre 10 y 30 platos y la estructura común.
		- Asociar un objeto `Thread` a cada repartidor para su ejecución.
	- Ejecutar los hilos.
	- Se suspende por 30 segundos para que se completen los pedidos.
	- Pasado este tiempo solicita la interrupción de los trabajos que no se hayan completado en ese tiempo.
	- Espera a que todos los hilos finalicen su ejecución antes de finalizar.


### Grupo 5
En el ejercicio se pretende simular la preparación de diferentes modelos para su impresión 3D en una granja de impresoras, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá la preparación de los modelos. Para la solución se tiene que utilizar los siguientes elementos ya programados:
- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.
- `Impresora3D`: Representa a una impresora 3D, tiene una cola de impresión de modelos a la espera, pero solo puede aceptar modelos que coincidan con la `CalidadImpresion` que puede generar la impresora. Además, por limitaciones de tiempo, una impresora no puede tener más de tres modelos asignados.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Ingeniero`: Representa a un trabajador que debe realizar una preparación de los modelos para su impresión. Tendrá al menos  las siguientes variables de instancia:
 - Un identificador.
 - Una lista de modelos para imprimir.
 - Una lista de impresoras para en encolar impresiones.
 - Los pasos que debe realizar son:
	 - Preparar la impresión: 
		 - La preparación podrá interrumpirse, cancelarse, pero el ingeniero debe de asegurarse de que se han preparado al menos todos los modelos de calidad `MEDICINA`.
		 - Procesa un modelo: lo inserta dentro de la cola de una impresora de su misma `CalidadImpresion`, intentando repartir el trabajo. Si no existen impresoras con la misma calidad o están llenas, se registrará la incidencia guardando el modelo en una estructura de datos común a todos los hilos.
		 - El modelo se considera procesado se pueda o no encolar en alguna impresora.
		 - Se simula un tiempo para preparar la impresión que estará definido por el propio modelo a través del correspondiente método.
		 - A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por el ingeniero, debe incluir como mínimo el porcentaje de modelos procesados, la información de las impresoras y debe quedar claro si el pedido ha sido cancelado/interrumpido.
		 - Los modelos no encolados por falta de tiempo **no se guardaran** en la estructura común. 
- `Hilo principal`: Realizará los siguientes pasos:
	- Crear 12 ingenieros:
		- Asignar a cada trabajador entre 7 y 11 impresoras, 20 modelos y la estructura para guardar los modelos que no han podido ser imprimidos.
		- Asociar un objeto `Thread` a cada ingeniero para su ejecución.
	- Ejecutar los hilos.
	- Se suspende por 30 segundos para que se completen las asignaciones.
	- Pasado este tiempo solicita la interrupción de los trabajos que no se hayan completado en ese tiempo.
	- Espera a que todos los hilos finalicen su ejecución antes de finalizar.
	- Imprime la información de los modelos no procesados.
	

### Grupo 6
En el ejercicio se pretende simular la vacunación de una serie de pacientes por enfermeros, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá la inyección de vacunas. Para la solución se tiene que utilizar los siguientes elementos ya programados:
- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `DosisVacuna`: Clase que representa a cada tipo de vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Enfermero`: Representa a un trabajador que debe realizar una vacunación a una serie de pacientes. Cada enfermero podrá tendrá disponible el doble de dosis que de pacientes.  Tendrá al menos  las siguientes variables de instancia:
 - Un identificador.
 - Una lista de pacientes para vacunar.
 - Una lista de vacunas disponibles compartida por todos los enfermeros.
 - Un contador para ir cogiendo las dosis, este contador esta compartido por todos los enfermeros y se proporciona en el proyecto base.
 - Los pasos que debe realizar son:
	 - Preparar la vacunación: 
		 - La preparación podrá interrumpirse, cancelarse, pero la interrupción se ignorará si el enfermero ha usado al menos el 80% de las dosis.
		 - Procesa una dosis: inyecta la dosis a un paciente válido, intentando maximizar el número de pacientes vacunados. Si no existen pacientes disponibles para la dosis, se registrará la incidencia guardando la dosis en una estructura de datos común a todos los hilos.
		 - Una dosis se considera usada en el momento que se llama a `getIndiceSiguienteDosis` haya podido inyectarse o no.
		 - Se simula un tiempo para aplicar la dosis que estará comprendido entre 1 y 3 segundos.
		 - A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por el enfermero, debe incluir como mínimo el número de pacientes, dosis administradas, dosis perdidas, la información de los pacientes, y debe quedar claro si el hilo ha sido cancelado/interrumpido.
		 - Las dosis no usadas por falta de tiempo **no se guardaran** en la estructura común. 
- `Hilo principal`: Realizará los siguientes pasos:
	- Crear 400 dosis en un array para compartir entre todos los enfermeros.
	- Crear 10 enfermeros:
		- Asignar a cada enfermero entre 10 y 15 pacientes, la estructura para acceder a las dosis disponibles y las dosis perdidas por no haber pacientes disponibles.
		- Asociar un objeto `Thread` a cada enfermero para su ejecución.
	- Ejecutar los hilos.
	- Se suspende por 30 segundos para que se completen las vacunaciones.
	- Pasado este tiempo solicita la interrupción de las vacunaciones que no se hayan completado en ese tiempo.
	- Espera a que todos los hilos finalicen su ejecución antes de finalizar.
	- Imprime la información de las vacunas usados no inyectadas.
